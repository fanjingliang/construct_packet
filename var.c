
#include "var.h"
#include <stdlib.h>
#include <string.h>
#include "global.h"
#include <stdint.h>
#include <ctype.h>

static var_table_handle_t* g_var_handle;


void var_table_reinit()
{
	int i;
	
	if(g_var_handle == NULL)
	{
		g_var_handle = (var_table_handle_t*)malloc(sizeof(var_table_handle_t));
	}
	else
	{
		for(i = 0 ; i < MAX_BUCKET_NUM ; i++)
		{
			var_node_t * pvar_node;
			list_node_t * head;
			list_node_t * node;

			head = &g_var_handle->head[i];
			
			while(!list_empty(head))
			{
				node = head->next;
				pvar_node = LIST_ENTRY(node,var_node_t,list);
				list_del_node(node);
				free(pvar_node);
			}
		}
	}
	
	memset(g_var_handle,0,sizeof(var_table_handle_t));

	for(i = 0 ; i < MAX_BUCKET_NUM ; i++)
	{
		list_node_t * head;
		head = &g_var_handle->head[i];

		INIT_LIST_HEAD(head);
	}
}

uint16_t var_table_hash (const char *pkey )
{
	uint32_t hash = 0;
	char *key = (char *) pkey;

	for (; *key; key++)
	{
		hash = (hash * 16777619) ^ (uint32_t) tolower(*key);
	}

	uint16_t * tmp = (uint16_t*) & hash;
	return tmp[0] ^ tmp[1];
}


var_node_t * var_table_find(const char * name)
{
	uint16_t hash = var_table_hash(name);

	var_node_t * pvar_node;
	list_node_t *head;
	list_node_t *node;
	
	head = &g_var_handle->head[hash];
	node = head->next;

	while(node != head)
	{
		pvar_node = LIST_ENTRY(node,var_node_t,list);
		if(strcmp(pvar_node->name,name) == 0)
		{
			return pvar_node;
		}
		node = node->next;
	}

	return NULL;
}

var_node_t * var_table_add(const char * name, const char *value)
{
	uint16_t hash = var_table_hash(name);

	var_node_t * pvar_node;
	list_node_t *head;

	pvar_node = (var_node_t*)malloc(sizeof(var_node_t));

	memset(pvar_node,0,sizeof(var_node_t));

	strcpy(pvar_node->name,name);
	strcpy(pvar_node->value,value);

	head = &g_var_handle->head[hash];

	list_add_tail(head,&pvar_node->list);
	
	return pvar_node;
}

void var_table_del(var_node_t * pvar_node)
{
	list_del_node(&pvar_node->list);
	free(pvar_node);
}

void var_table_set(const char * name, const char * value)
{
	var_node_t * pvar_node;

	pvar_node = var_table_find(name);
	if(pvar_node == NULL)
	{
		var_table_add(name,value);
	}
	else
	{
		strcpy(pvar_node->value,value);
	}
}

void var_table_unset(const char * name)
{
	var_node_t * pvar_node;

	pvar_node = var_table_find(name);
	if(pvar_node != NULL)
	{
		var_table_del(pvar_node);
	}
}

char * var_table_get(const char * name)
{
	var_node_t * pvar_node;

	pvar_node = var_table_find(name);
	if(pvar_node != NULL)
	{
		return pvar_node->value;
	}
	else
	{
		return "";
	}
}

