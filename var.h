
#pragma once

#include "list.h"
#include <stdint.h>
#include "define.h"


typedef struct
{
	char name[MAX_NAME_LEN];
	char value[MAX_VALUE_LEN];
	list_node_t list;
} var_node_t;


typedef struct
{
	list_node_t head[MAX_BUCKET_NUM];
} var_table_handle_t;



void var_table_reinit();


uint16_t var_table_hash (const char *pkey );
var_node_t * var_table_find(const char * name);
var_node_t * var_table_add(const char * name, const char *value);
void var_table_del(var_node_t * pvar_node);
void var_table_set(const char * name, const char * value);
void var_table_unset(const char * name);
char * var_table_get(const char * name);

