
#include <stdlib.h>

#include "global.h"
#include <string.h>
#include <dirent.h>
#include "alias.h"

void load_layer_line( layer_t * play, const char * parts[], int num)
{
	const char * name = parts[0];
	int otype = field_fixed;
	int len = atoi(parts[1]);
	int bitlen = atoi(parts[2]);
	int vtype;
	int dtype;
	const char * def_val ;
	if(strcmp(parts[3],"checksum") == 0)
	{
		vtype = value_type_checksum;
	}
	else if(strcmp(parts[3] ,"len") == 0)
	{
		vtype = value_type_len_include_head_len;
	}
	else if(strcmp(parts[3],"lennot") == 0)
	{
		vtype = value_type_len_notinclude_head_len;
	}
	else if(strcmp(parts[3],"set") == 0)
	{
		vtype = value_type_set;
	}
	else
	{
		vtype = value_type_default;
		def_val = parts[3];
	}

	if(strcmp(parts[4],"uint8_t") == 0)
	{
		dtype = value_data_type_uint8;
		
	}
	else if(strcmp(parts[4],"uint16_t") == 0)
	{
		dtype = value_data_type_uint16;
		
	}
	else if(strcmp(parts[4],"uint32_t") == 0)
	{
		dtype = value_data_type_uint32;
		
	}
	else if(strcmp(parts[4],"uint64_t") == 0)
	{
		dtype = value_data_type_uint64;
		
	}
	else if(strcmp(parts[4],"ipv4") == 0)
	{
		dtype = value_data_type_ipv4;
		
	}
	else if(strcmp(parts[4],"ipv6") == 0)
	{
		dtype = value_data_type_ipv6;
	}
	else if(strcmp(parts[4],"bcd") == 0)
	{
		dtype = value_data_type_bcd;
	}
	else if(strcmp(parts[4],"var") == 0)
	{
		dtype = value_data_type_hexvar;
	}
	else 
	{
		dtype = value_data_type_hexstring;
	}
	
	field_t * pfield = create_field(name, otype,len, bitlen,vtype,dtype,def_val);

	add_field(play,pfield);
	
}

void load_layer_info(const char * library, const char * file)
{
	FILE * fp;

	fp = fopen(file,"r");

	if(fp == NULL) return;

	char linetmp[1024] = {0};
	char line[1024] = {0};
	char * ret;

	char  * pdot;
	pdot = strchr(file,'.');
	pdot = pdot + 1;

	layer_t * play;
	play = location_layer(pdot);
	
	if(play != NULL)
	{
		printf("\tERR: layer %s is exist\n",pdot);
		fclose(fp);
		return;
	}
	printf("OK: load layer [%s]\n",pdot);
	play = create_layer(pdot);
	add_layer(play);

	strcpy(play->library,library);
	
	char description[4096] = {0};

	while( (ret=fgets(linetmp,sizeof(linetmp)-1,fp)) != NULL)
	{
		int len = strlen(linetmp);
		int i;
		int j;
		int has_space = 1;
		
		for(i = 0,j=0 ; i < len ; i ++)
		{
			if(linetmp[i] == ' ' || linetmp[i] == '\t')
			{
				if(has_space == 0)
				{
					line[j] = ' ';
					j++;
					has_space = 1;
				}
				else
				{
					continue;
				}
			}
			else if(linetmp[i] == '\r' || linetmp[i] == '\n')
			{
				continue;
			}
			else
			{
				line[j] = linetmp[i];
				has_space = 0;
				j ++;
			}
		}
		line[j] = 0;

		if(strlen(line) == 0) continue;

		if(line[0] == '#')
		{
			strcat(description,line);
			continue;
		}


		const char * parts[10] = {0};
		int parts_num;

		parts_num = 0;
		
		char * ret = strtok(line, " ");
		while(ret != NULL)
		{
			parts[parts_num] = ret;
			parts_num ++;
			ret = strtok(NULL, " ");
		}
		
		load_layer_line(play,parts,parts_num);
			
	}
	fclose(fp);

	strcpy(play->description, description);
}

void load_all_layer_info(const char * path)
{
	
	struct dirent **namelist;
	int n;

	n = scandir(path, &namelist, 0, alphasort);
	if (n < 0)
		perror("scandir");
	else
	{
		while(n--)
		{
			if(strncmp(namelist[n]->d_name, "config.", 7) == 0)
			{
				char buff[256] = {0};
				sprintf(buff,"%s/%s",path,namelist[n]->d_name);
				load_layer_info(path, buff);
			}
			free(namelist[n]);
		}
		free(namelist);
	}
}

void load_alias(const char * file)
{
	FILE * fp;

	fp = fopen(file,"r");

	if(fp == NULL) return;

	char  * pdot;
	pdot = strchr(file,'.');
	pdot = pdot + 1;

	alias_t * palias;
	palias = location_alias(pdot);
	
	if(palias != NULL)
	{
		printf("\tERR: alias %s is exist\n",pdot);
		fclose(fp);
		return;
	}
	printf("OK: load alias [%s]\n",pdot);
	palias = create_alias(pdot);
	add_alias(palias);

	fread(palias->script_buf,1,4096,fp);
	fclose(fp);
}

void load_all_alias(const char * path)
{
	
	struct dirent **namelist;
	int n;

	n = scandir(path, &namelist, 0, alphasort);
	if (n < 0)
		perror("scandir");
	else
	{
		while(n--)
		{
			if(strncmp(namelist[n]->d_name, "alias.", 6) == 0)
			{
				char buff[256] = {0};
				sprintf(buff,"%s/%s",path,namelist[n]->d_name);
				load_alias(buff);
			}
			free(namelist[n]);
		}
		free(namelist);
	}
}


