
#include <unistd.h>
#include "config.h"
#include "global.h"
#include "script.h"
#include "mysend.h"
#include "command.h"


int main(int argc, char * argv[])
{
	running_init();
	load_all_layer_info("config");
	load_all_layer_info("config/gtp-v2");
	load_all_alias("alias");

	//加载上次退出时的自动保存
	do_scanf_file("autosave.cmd");
	printf("======================================\n");
	do_scanf_stdin();

	return 0;
}
