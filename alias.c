
#include "alias.h"
#include <stdlib.h>
#include <string.h>
#include "global.h"

static list_node_t g_aliases;

void init_alias()
{
	INIT_LIST_HEAD(&g_aliases);
}

alias_t * create_alias(const char * name)
{
	alias_t * p = (alias_t*)malloc(sizeof(alias_t));

	memset(p,0,sizeof(alias_t));

	strcpy(p->name , name);
	
	return p;
}


void add_alias( alias_t * new_alias)
{
	list_add_tail(&g_aliases, &new_alias->list);
}

alias_t * location_alias( const char * name)
{
	list_node_t *head;
	list_node_t *node;

	head = &g_aliases;

	node = head->next;

	while(node != head)
	{
		alias_t *palias;

		palias = LIST_ENTRY(node,alias_t,list);
		if(strcmp(palias->name,name)==0)
		{
			return palias;
		}
		node = node->next;
	}
	return NULL;
}

