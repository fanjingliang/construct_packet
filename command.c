
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>


#include <unistd.h>
#include "config.h"
#include "list.h"
#include "global.h"
#include "pcap.h"
#include "script.h"
#include "mysend.h"
#include "tool.h"
#include "command.h"
#include "display.h"

cmd_node_t * g_cmd_list = NULL;


void reg_command( CMD_FUNCTION func, CMD_HELP help )
{
	cmd_node_t * pnew = (cmd_node_t *) malloc(sizeof(cmd_node_t));

	memset(pnew,0,sizeof(cmd_node_t));

	help(&pnew->name, &pnew->param_list, &pnew->brief_desc, &pnew->detailed_desc);
	
	pnew->func = func;

	pnew->next = g_cmd_list;
	g_cmd_list = pnew;
}

void do_reg_commands()
{
	reg_command(cmd_create_packet, cmd_create_packet_help);
	reg_command(cmd_create_layer,cmd_create_layer_help);
	reg_command(cmd_create_layers,cmd_create_layers_help);
	reg_command(cmd_create_layer_child,cmd_create_layer_child_help);
	reg_command(cmd_create_layer_children,cmd_create_layer_children_help);
	reg_command(cmd_select_layer_path,cmd_select_layer_path_help);
	reg_command(cmd_info_packets,cmd_info_packets_help);
	reg_command(cmd_info_layer_type,cmd_info_layer_type_help);
	
	reg_command(cmd_set_value,cmd_set_value_help);
	reg_command(cmd_send_packet,cmd_send_packet_help);
	reg_command(cmd_write_all,cmd_write_all_help);
	reg_command(cmd_write_packet_command,cmd_write_packet_command_help);
	reg_command(do_scanf_file);
	reg_command(cmd_open_device,cmd_open_device_help);
	reg_command(cmd_close_device,cmd_close_device_help);

	reg_command(cmd_open_pcap_file,cmd_open_pcap_file_help);
	reg_command(cmd_close_pcap_file,cmd_close_pcap_file_help);
	reg_command(cmd_info_flow_content,cmd_info_flow_content_help);

	return 0;
}

int do_command(char * line)
{
	char command[1280] = {0};
	char param_a[1280] = {0};
	char param_b[1280] = {0};
	char param_c[1280] = {0};
	char param_d[1280] = {0};
	char param_e[1280] = {0};
	char param_f[1280] = {0};
	char param_g[1280] = {0};
	char param_h[1280] = {0};
	
	char *param[] = {
		param_a, param_b, param_c, param_d, param_e, 
		param_f, param_g, param_h
		};
	int param_num = 0;
	
	param_num = sscanf(pline,"%s %s %s %s %s %s %s %s %s %s",command,
			param_a,param_b,param_c,param_d,param_e,param_f,param_g,param_h);

	cmd_node_t * p = g_cmd_list;
	while(p != NULL)
	{
		if( strcmp(p->name, command) == 0)
		{
			return p->func(param_num, param);
		}

		p = p->next;
	}

	if(param_num > 0)
	{
		printf("command is not found\n");
	}

	return CMD_RET_OK;
}

void do_scanf_stdin()
{
	char * pline;
	while( (pline = readline ("==>")) != NULL)	
	{
		int ret;
		ret = do_command(pline);

		switch(ret)
		{
		case RET_OK:
		case RET_ERR:
		case RET_QUIT:
		}
	}

}

void do_scanf_file(const char * file)
{
	char line[40960] = {0};

	FILE *fp;
	fp = fopen(file,"r");

	if(fp != NULL)
	{
		int ret;
		
		while(fgets(line,8192,fp) != NULL)
		{
			ret = do_command(line);
			if(ret != RET_OK)
				break;
		}
		
		fclose(fp);
	}
}


void do_scanf_buf(char *buf)
{
	FILE *fp;
	fp = fopen("/tmp/tttt.txt","w");
	fwrite(buf,1,strlen(buf),fp);
	fclose(fp);

	do_scanf_file("/tmp/tttt.txt");
}


