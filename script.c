
#include <string.h>
#include <stdlib.h>
#include "packet.h"
#include "global.h"
#include "tool.h"
#include "script.h"
#include "mysend.h"
#include "alias.h"
#include "display.h"

static pcap_dumper_t* g_pcap_pdump = NULL;
static int g_device_id = 0;

static char command_err_info[128] = {0};

void command_set_err(const char *fmt, ...)
{
	va_list args;
	va_start (args, fmt);
	vsnprintf(command_err_info ,sizeof(command_err_info)-1,fmt,args);
	va_end(args);
}

char * command_get_err()
{
	return command_err_info;
}

/*
	显示所有的包的结构信息
*/
void cmd_info_packets(int argc, char *argv[])
{
	
	list_node_t *head;
	list_node_t *node;

	head = running_get_packet_list_head();
	node = head->next;

	printf("---------------------------------------------------------------");
	while(node != head)
	{
		packet_running_t * ppkt;
		
		ppkt = LIST_ENTRY(node,packet_running_t, list);

		if(ppkt == current_packet_ptr)
			printf("[%s]",ppkt->name);
		else
			printf("%s",ppkt->name);
		
		node = node->next;
		
	}
	
	printf("---------------------------------------------------------------");
	return CMD_RET_OK;
}

/*
显示某个层的详细信息
*/
void cmd_info_layer_type(int argc, char *argv[])
{
	layer_t *play =  location_layer( argv[0] );

	if(play == NULL) return CMD_RET_ERR;

	list_node_t *head;
	list_node_t *node;

	head = &play->field_list;
	node = head->next;

	printf("---------------------------------------------------------------");
	printf("layer : %s", name);
	printf("library : %s", play->library);
	printf("description:");
	printf("%s",play->description);
	printf("---------------------------------------------------------------");
	while(node != head)
	{
		field_t * pfield;
		pfield = LIST_ENTRY(node,field_t, list);
		//--------------------------------------------------------------------
		//显示字段信息
		printf("\t%s\t%d:%d",pfield->name,pfield->len,pfield->bitlen);
		//--------------------------------------------------------------------
		node = node->next;
	}
	printf("---------------------------------------------------------------");
	return CMD_RET_OK;
	
}

void __cmd_info_layer_content(layer_running_t * play, int level)
{
	char tabbuf[1024] = {0};
	char fmt[128] = {0};
	sprintf(fmt,"%%-%ds",level*4);
	sprintf(tabbuf,fmt," ");

	printf("%s{ %s",tabbuf,play->layer_info->name);

	///////////////////////////////////////////////////////
	list_node_t * field_list_head;
	list_node_t * field_list_node;
	
	field_list_head = &play->field_list;
	field_list_node = field_list_head->next;

	char line[1024] = {0};
	char tmp[128] = {0};
	while(field_list_node != field_list_head)
	{
		field_running_t * pfield = NULL;
		field_t * pfield_info = NULL;
		pfield = LIST_ENTRY(field_list_node,field_running_t, list);
		pfield_info = pfield->field_info;
		sprintf(tmp,"(%s:%s),",pfield_info->name, pfield->value_from_to);
		strcat(line,tmp);
		if(strlen(line) > 80)
		{
			printf("%s    %s",tabbuf,line);
			line[0] = 0;
		}
		field_list_node = field_list_node->next;
	}
	if(strlen(line) > 0)
		printf("%s    %s",tabbuf,line);
	///////////////////////////////////////////////////////
	list_node_t *layer_list_head;
	list_node_t *layer_list_node;
	layer_list_head = &play->child_list;
	layer_list_node = layer_list_head->next;

	while(layer_list_node != layer_list_head)
	{
		layer_running_t * pchild;
		pchild = LIST_ENTRY(layer_list_node,layer_running_t, list);

		_cmd_info_layer_content(pchild, level+1);
		layer_list_node = layer_list_node->next;
	}
	///////////////////////////////////////////////////////

	printf("%s},",tabbuf);
}


void cmd_info_packet_content(int argc, char *argv[])
{
	packet_running_t * ppkt =  running_get_current_packet();
	
	if(ppkt == NULL) 
	{
		command_set_err("\tERR: packet not exist");
		return CMD_RET_ERR;
	}

	printf("---------------------------------------------------------------");
	printf(" all the particulars of [%s]", ppkt->name);
	printf("---------------------------------------------------------------");
	
	list_node_t *layer_list_head;
	list_node_t *layer_list_node;

	layer_list_head = &ppkt->layer_list;
	layer_list_node = layer_list_head->next;

	while(layer_list_node != layer_list_head)
	{
		layer_running_t * play;
		play = LIST_ENTRY(layer_list_node,layer_running_t, list);

		__cmd_info_layer_content(play, 1);
		layer_list_node = layer_list_node->next;
	}

	return CMD_RET_OK;
}


int cmd_create_packet(int argc, char *argv[])
{
	bool ret;

	if(argc < 1) 
	{
		command_set_err("\tERR: need a param\n");
		return CMD_RET_ERR;
	}
	
	ret = running_create_packet(argv[0]);

	if(ret == false)
	{
		command_set_err("\tERR: flow is exist error");
		return CMD_RET_ERR;
	}
	else
	{
		return CMD_RET_OK;
	}
}

void cmd_select_packet(int argc, char *argv[])
{
	bool ret;
	ret = running_select_packet(argv[0]);

	if(ret == false)
	{
		command_set_err("\tERR: flow name error");
		return CMD_RET_ERR;
	}
	else
	{
		return CMD_RET_OK;
	}
}

void cmd_create_layer(int argc, char *argv[])
{
	packet_running_t * pkt = running_get_current_packet();
	if( pkt == NULL)
	{
		command_set_err("\tERR: select flow first");
		return CMD_RET_ERR;
	}

	layer_running_t * pnew = NULL;

	if(pkt->current_layer == NULL)
	{
		pnew =	running_add_packet_layer(pkt,argv[0]);
		if(pnew == NULL)
		{
			command_set_err("\tERR: layer name error");
			return CMD_RET_ERR;
		}
	}
	else
	{
		pnew =	running_add_layer_next(current_layer_ptr,argv[0]);
		if(pnew != NULL)
			current_layer_ptr = pnew;
		else
		{
			command_set_err("\tERR: layer name error");
			return CMD_RET_ERR;
		}
	}
	return CMD_RET_OK;
	
}


void cmd_create_layers(int argc, char *argv[])
{
	char * layer_type_name[128] = {0};
	int n = 0;

	
	if(current_packet_ptr== NULL)
	{
		command_set_err("\tERR: select flow first");
		return CMD_RET_ERR;
	}
	
	n = split_string(argv[0], "/", layer_type_name);

	int i;
	
	for(i = 0 ; i < n ; i++)
	{
		layer_t * pnew;
		pnew = location_layer(layer_type_name[i]);
		if(pnew == NULL)
		{
			command_set_err("\tERR: layer %s error", layer_type_name[i]);
			free_split_string(n,layer_type_name);
			return CMD_RET_ERR;
		}
	}

	for(i = 0 ; i < n ; i++)
	{
		if(current_layer_ptr == NULL)
		{
			current_layer_ptr = running_add_packet_layer(current_packet_ptr,layer_type_name[i]);
		}
		else
		{
			current_layer_ptr = running_add_layer_next(current_layer_ptr,layer_type_name[i]);
		}
	}

	free_split_string(n,layer_type_name);
	return CMD_RET_OK;
}

void cmd_create_layer_child(int argc, char *argv[])
{
	if(current_layer_ptr== NULL)
	{
		command_set_err("\tERR: select layer first");
		return CMD_RET_ERR;
	}

	layer_running_t * pnew = NULL;
	pnew = running_add_layer_child(current_layer_ptr,argv[0]);

	if(pnew != NULL)
	{
		current_layer_ptr = pnew;
		return CMD_RET_OK;
	}
	else
	{
		command_set_err("\tERR: layer name error");
		return CMD_RET_ERR;
	}
}

void cmd_create_layer_children(int argc, char *argv[])
{
	char * layer_type_name[128] = {0};
	int n = 0;

	
	if(current_layer_ptr== NULL)
	{
		command_set_err("\tERR: select layer first");
		return;
	}
	
	n = split_string(argv[0], "/", layer_type_name);

	
	int i;
	
	for(i = 0 ; i < n ; i++)
	{
		layer_t * pnew;
		pnew = location_layer(layer_type_name[i]);
		if(pnew == NULL)
		{
			command_set_err("\tERR: layer %s error", layer_type_name[i]);
			free_split_string(n,layer_type_name);
			return CMD_RET_ERR;
		}
	}

	layer_running_t * pnewrun;

	for(i = 0 ; i < n ; i++)
	{
		pnewrun = running_add_layer_child(current_layer_ptr,layer_type_name[i]);
	}

	if(pnewrun != NULL)
	{
		current_layer_ptr = pnewrun;
	}
	free_split_string(n,layer_type_name);
	return CMD_RET_OK;
}


void cmd_select_layer_path(int argc, char *argv[])
{
	char * layer_type_name[128] = {0};
	int n = 0;
	
	if(current_packet_ptr== NULL)
	{
		command_set_err("\tERR: select flow first");
		return CMD_RET_ERR;
	}
	
	n = split_string(argv[0], "/", layer_type_name);

	int i;
	char layername[128]= {0};
	char layerindex = 0;

	layer_running_t * ppos = NULL;
	for(i = 0 ; i < n ; i++)
	{
		char *p ,*q;
		p = strchr(layer_type_name[i],'[');
		if(p == NULL)
		{
			strcpy(layername,layer_type_name[i]);
			layerindex = 0;
		}
		else
		{
			strncpy(layername,layer_type_name[i], p-layer_type_name[i]);
			q = strchr(p,']');
			if(q != NULL)
			{
				char tmp[128] = {0};
				strncpy(tmp,p+1,q-(p+1));
				layerindex = atoi(tmp);
			}
		}

		list_node_t *head;
		list_node_t *node;
		layer_running_t * play;
		
		if(ppos == NULL)
		{
			head = &current_packet_ptr->layer_list;
		}
		else
		{
			head = &ppos->child_list;
		}
			node = head->next;
			
		int j = 0;
		
		while(node != head)
		{
			play = LIST_ENTRY(node,layer_running_t, list);

			if(strcmp(play->layer_info->name, layername) == 0)
			{
				if(j == layerindex)
				{
					ppos = play;
					break;
				}
				else
				{
					j++;
				}
			}
			node = node->next;
		}
			
		
		if(ppos == NULL)
		{
			command_set_err("\tERR: location error");
			free_split_string(n,layer_type_name);
			return CMD_RET_ERR;
		}
	}

	ppos->packet->current_layer = ppos;
	
	free_split_string(n,layer_type_name);
	return CMD_RET_OK;
}

void cmd_set_value(int argc, char *argv[])
{
	field_running_t * pleftfield;

	pleftfield = running_location_field(current_layer_ptr, argv[0]);

	if(pleftfield == NULL)
	{
		command_set_err("\tERR: field name error");
		return CMD_RET_ERR;
	}


	strcpy(pleftfield->value_from_to,argv[1]);
	return CMD_RET_OK;
}

void cmd_set_payload_type(int argc, char *argv[])
{
	if(strcmp(argv[0],"a-z")==0)
	{
		current_packet_ptr->payload_type = PAYLOAD_TYPE_A_Z;
	}
	else if(strcmp(argv[0],"random")==0)
	{
		current_packet_ptr->payload_type = PAYLOAD_TYPE_RANDOM;
	}
	else
	{
		current_packet_ptr->payload_type = PAYLOAD_TYPE_SET;
	}
	return CMD_RET_OK;
}


void cmd_open_pcap_file(int argc, char *argv[])
{
	if(g_pcap_pdump != NULL)
	{
		dumpfile_close(g_pcap_pdump);
	}

	g_pcap_pdump = dumpfile_open(argv[0]);

	return CMD_RET_OK;
}

void cmd_open_device(int argc, char *argv[])
{
	if(g_device_id != 0)
	{
		close(g_device_id);
	}
	g_device_id = my_socket_init(argv[0]);
	return CMD_RET_OK;
}

void cmd_close_pcap_file(int argc, char *argv[])
{
	if(g_pcap_pdump != NULL)
	{
		dumpfile_close(g_pcap_pdump);
		g_pcap_pdump = NULL;
	}
	return CMD_RET_OK;
}

void cmd_close_device(int argc, char *argv[])
{
	if(g_device_id != 0)
	{
		close(g_device_id);
		g_device_id = 0;
	}
	return CMD_RET_OK;
}

void __cmd_sendto_device(char *buf ,int len)
{
	return CMD_RET_OK;
}

void __cmd_sendto_pcap_file(char *buf ,int len)
{
	if(g_pcap_pdump != NULL)
	{
		struct pcap_pkthdr dumph;
		memset(&dumph,0,sizeof(dumph));

		dumph.caplen = len;
		dumph.len = len;
		pcap_dump((u_char*)g_pcap_pdump,&dumph, (void*) buf);		
	}
	return CMD_RET_OK;
}

void cmd_send_packet(int argc, char *argv[])
{
	char buf[4096] = {0};
	int len = 0;

	
	packet_running_t * ppkt;
	
	ppkt = running_find_packet(argv[0]);
	if(ppkt == NULL)
	{
		command_set_err("\tERR: flow name error");
		return CMD_RET_ERR;
	}

	int n;

	n = running_check_max_packet(ppkt);
	
	if(strlen(argv[1]) > 0)
		n = atol(argv[1]);

	while(n--)
	{
		running_step_packet(ppkt);
		
		running_compute_packet(ppkt);

		running_construct_packet(ppkt,buf,&len);
		__cmd_sendto_device(buf,len);
		__cmd_sendto_pcap_file(buf,len);
	}
	return CMD_RET_OK;
}


void __cmd_write_update_layer_N(packet_running_t * ppkt,layer_running_t * play)
{
	list_node_t *head;
	list_node_t *node;

	if(play->parent == NULL)
		head = &ppkt->layer_list;
	else
		head = &play->parent->child_list;
		
	node = head->next;

	int index = 0;
	
	while(node != head)
	{
		layer_running_t * p;
		
		p = LIST_ENTRY(node,layer_running_t, list);

		if(strcmp(play->layer_info->name,p->layer_info->name) == 0)
		{
			if(play == p)
			{
				play->N = index;
				return;
			}
			else
			{
				index ++;
			}
		}
		node = node->next;
	}
}
void __cmd_write_layer(packet_running_t * ppkt,layer_running_t * play, FILE *fp)
{
	//生成选择层命令
	char layername[128][128] = {0};
	int n = 0;
	layer_running_t * p;
	p = play;
	while( p!= NULL)
	{
		cmd_write_update_layer_N(ppkt,p);
		sprintf(layername[n],"%s[%d]",p->layer_info->name,p->N);
		n ++;
		p = p->parent;
	}

	fprintf(fp,"select-layer-path ");
	while(n--)
	{
		fprintf(fp,"%s/",layername[n]);
	}
	fprintf(fp,"\n");

	//生成设置字段命令
	list_node_t *fhead;
	list_node_t *fnode;
	
	fhead = &play->field_list;
	fnode = fhead->next;
	
	while(fnode != fhead)
	{
		field_running_t * pfield;
		
		pfield = LIST_ENTRY(fnode,field_running_t, list);

		if(pfield->field_info->vtype == value_type_default)
		{
			if(strcmp(pfield->field_info->default_value,pfield->value_from_to) != 0)
			{
				fprintf(fp,"set  %s %s\n",pfield->field_info->name, pfield->value_from_to);
			}
		}
		else
		{
			if(strlen(pfield->value_from_to) > 0)
			{
				fprintf(fp,"set  %s %s\n",pfield->field_info->name, pfield->value_from_to);
			}
		}
		fnode = fnode->next;
	}

	//生成子层结构
	list_node_t *head;
	list_node_t *node;

	head = &play->child_list;
	node = head->next;
	if(!list_empty(head))
	{
		fprintf(fp,"add-layer-children ");
		while(node != head)
		{
			layer_running_t * pchildlay;
			
			pchildlay = LIST_ENTRY(node,layer_running_t, list);
			fprintf(fp,"%s/",pchildlay->layer_info->name);
			node = node->next;
		}
		fprintf(fp,"\n");
	}
	
	//递归生成子层的内容
	head = &play->child_list;
	node = head->next;

	while(node != head)
	{
		layer_running_t * pchildlay;
		
		pchildlay = LIST_ENTRY(node,layer_running_t, list);
		_cmd_write_layer(ppkt,pchildlay,fp);
		node = node->next;
	}
	
}

void __cmd_write_packet(packet_running_t * ppkt , FILE * fp)
{
	//创建流
	fprintf(fp,"\nadd-flow %s\n", ppkt->name);

	list_node_t *head;
	list_node_t *node;

	//创建层次列表

	//创建顶层结构
	head = &ppkt->layer_list;
	node = head->next;

	if(!list_empty(head))
		fprintf(fp,"add-layers ");
	while(node != head)
	{
		layer_running_t * play;
		
		play = LIST_ENTRY(node,layer_running_t, list);
		fprintf(fp,"%s/", play->layer_info->name);
		node = node->next;
	}
	fprintf(fp,"\n");
	
	head = &ppkt->layer_list;
	node = head->next;

	while(node != head)
	{
		layer_running_t * play;
		
		play = LIST_ENTRY(node,layer_running_t, list);

		_cmd_write_layer(ppkt,play,fp);
		node = node->next;
	}

}

void cmd_write_packet(int argc, char *argv[])
{
	packet_running_t * ppkt =  running_location_packet(argv[0]);
	
	if(ppkt == NULL) 
	{
		command_set_err("\tERR: flow name error\n\n");	
		return CMD_RET_ERR;
	}

	FILE * fp = stdio;
	if(argc >=2 )
	{
		fp = fopen(argv[1],"w");
		if(fp == NULL)
		{
			command_set_err("\tERR: open file fail\n\n");	
			return CMD_RET_ERR;
		}
	}

	_cmd_write_packet(ppkt, fp);

	if(fp != stdio)
		fclose(fp);

	return CMD_RET_OK;
}

//当程序退出时自动保存所有的内容
void cmd_write_all(int argc, char *argv[])
{
	FILE * fp = stdio;
	if(argc >= 1 )
	{
		fp = fopen(argv[0],"w");
		if(fp == NULL)
		{
			command_set_err("\tERR: open file fail\n\n");	
			return CMD_RET_ERR;
		}
	}

	list_node_t *head;
	list_node_t *node;

	head = running_get_packet_list_head();
	node = head->next;

	while(node != head)
	{
		packet_running_t * ppkt;
		
		ppkt = LIST_ENTRY(node,packet_running_t, list);

		_cmd_write_packet(ppkt->name,fp);			
		node = node->next;
	}

	if(fp != stdio)
		fclose(fp);

	return CMD_RET_OK;
		
}

void __cmd_delete_packet(packet_running_t * ppkt)
{
}

void cmd_delete_packet(int argc, char *argv[])
{
	packet_running_t * ppkt =  running_delete_packet(argv[0]);
		
	if(ppkt == NULL) 
	{
		command_set_err("\tERR: flow name error\n\n");	
		return CMD_RET_ERR;
	}

	__cmd_delete_packet(ppkt);
	return CMD_RET_ERR;
}

