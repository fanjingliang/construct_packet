
#include <unistd.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>  
#include <pcap.h>
#include <string.h>
#include <stdlib.h>

#include "tool.h"

unsigned short checksum(unsigned short *buf, int nword)
{
    unsigned long sum ;

    for(sum = 0; nword > 0; nword--)
        sum += *buf++;

    sum = (sum>>16) + (sum&0xffff);
    sum += (sum>>16);

    return ~sum;
}

uint64_t hton64(uint64_t q)
{
	register uint32_t u, l;
	u = q >> 32;
	l = (uint32_t) q;

	return htonl(u) | ((uint64_t)htonl(l) << 32);
}

uint32_t ipv4_string_2_int32(const char * ipv4str)
{
	struct in_addr network;

	int ret = inet_aton (ipv4str, &network);
	if (! ret)
		return 0;

	return network.s_addr;
}
/** int
 * inet_pton4(src, dst)
 *  like inet_aton() but without all the hexadecimal and shorthand.
 * return:
 *  1 if `src' is a valid dotted quad, else 0.
 * notice:
 *  does not touch `dst' unless it's returning 1.
 * author:
 *  Paul Vixie, 1996.
 */
int inet_pton4(const char *src, unsigned char *dst)
{
    static const char digits[] = "0123456789";
    int saw_digit, octets, ch;
    unsigned char tmp[4], *tp;
 
    saw_digit = 0;
    octets = 0;
    *(tp = tmp) = 0;
    while ((ch = *src++) != '\0') {
    const char *pch;
 
    if ((pch = strchr(digits, ch)) != NULL) {
        unsigned int new = *tp * 10 + (unsigned int)(pch - digits);
 
        if (new > 255)
        return (0);
        *tp = new;
        if (! saw_digit) {
        if (++octets > 4)
            return (0);
        saw_digit = 1;
        }
    } else if (ch == '.' && saw_digit) {
        if (octets == 4)
        return (0);
        *++tp = 0;
        saw_digit = 0;
    } else
        return (0);
    }
    if (octets < 4)
    return (0);
 
    memcpy(dst, tmp, 4);
    return (1);
}
 

/** int
 * inet_pton6(src, dst)
 *  convert presentation level address to network order binary form.
 * return:
 *  1 if `src' is a valid [RFC1884 2.2] address, else 0.
 * notice:
 *  (1) does not touch `dst' unless it's returning 1.
 *  (2) :: in a full address is silently ignored.
 * credit:
 *  inspired by Mark Andrews.
 * author:
 *  Paul Vixie, 1996.
 */
int inet_pton6(const char *src, unsigned char *dst)
{
    static const char xdigits_l[] = "0123456789abcdef",
              xdigits_u[] = "0123456789ABCDEF";
    unsigned char tmp[16], *tp, *endp, *colonp;
    const char *xdigits, *curtok;
    int ch, saw_xdigit;
    unsigned int val;
 
    memset((tp = tmp), '\0', 16);
    endp = tp + 16;
    colonp = NULL;
    /** Leading :: requires some special handling. */
    if (*src == ':')
        if (*++src != ':')
            return (0);
    curtok = src;
    saw_xdigit = 0;
    val = 0;
    while ((ch = *src++) != '\0') {
        const char *pch;
 
        if ((pch = strchr((xdigits = xdigits_l), ch)) == NULL)
            pch = strchr((xdigits = xdigits_u), ch);
        if (pch != NULL) {
            val <<= 4;
            val |= (pch - xdigits);
            if (val > 0xffff)
                return (0);
            saw_xdigit = 1;
            continue;
        }
        if (ch == ':') {
            curtok = src;
            if (!saw_xdigit) {
                if (colonp)
                    return (0);
                colonp = tp;
                continue;
            }
            if (tp + 2 > endp)
                return (0);
            *tp++ = (unsigned char) (val >> 8) & 0xff;
            *tp++ = (unsigned char) val & 0xff;
            saw_xdigit = 0;
            val = 0;
            continue;
        }
        if (ch == '.' && ((tp + 4) <= endp) &&
            inet_pton4(curtok, tp) > 0) {
            tp += 4;
            saw_xdigit = 0;
            break;  /** '\0' was seen by inet_pton4(). */
        }
        return (0);
    }
    if (saw_xdigit) {
        if (tp + 2 > endp)
            return (0);
        *tp++ = (unsigned char) (val >> 8) & 0xff;
        *tp++ = (unsigned char) val & 0xff;
    }
    if (colonp != NULL) {
        /**
         * Since some memmove()'s erroneously fail to handle
         * overlapping regions, we'll do the shift by hand.
         */
        const size_t n = tp - colonp;
        size_t i;
 
        for (i = 1; i <= n; i++) {
            endp[- i] = colonp[n - i];
            colonp[n - i] = 0;
        }
        tp = endp;
    }
    if (tp != endp)
        return (0);
    memcpy(dst, tmp, 16);
    return (1);
}

void ipv6_string_2_uint64(const char *src, unsigned char *dst)
{
	inet_pton6(src,dst);
}

void hexstring_2_buf(const unsigned char * hexstring, unsigned char *outbuf, int len)
{
	if(strlen(hexstring)%2 != 0)
		return;

	static unsigned char v[] = {
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 
		0, 0xA,0xB, 0xC, 0xD, 0xE, 0xF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	};

	int i;
	for( i = 0 ; i < len; i++)
	{
		unsigned char tmp = 0;
		
		tmp = v[hexstring[i*2]];
		tmp = tmp << 4 ;
		tmp |= v[hexstring[i*2 + 1]];

		outbuf[i] = tmp;
	}
}

/*
 * 将16进制的字符串转为内存格式，并返回实际的字节数
*/
void hexstring_2_buf_2(const unsigned char * hexstring, unsigned char *outbuf, int *len)
{
	static unsigned char v[] = {
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 
		0, 0xA,0xB, 0xC, 0xD, 0xE, 0xF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	};

	*len = strlen((char *)hexstring)/2;

	int i;
	for( i = 0 ; i < *len; i++)
	{
		unsigned char tmp = 0;
		
		tmp = v[hexstring[i*2]];
		tmp = tmp << 4 ;
		tmp |= v[hexstring[i*2 + 1]];

		outbuf[i] = tmp;
	}
}

void bcd_string_2_buf(const char * bcdstr, char *out, int len)
{
	int i;
	int j;
	uint8_t c1,c2 ;

	int bcdlen = strlen(bcdstr);
	
	for(i = 0,j=0 ; i < len ; i++)
	{
		c1 = 0;
		if(j < bcdlen)
			c1 = bcdstr[j] - '0';
		else
			c1 = 0xf;
		j++;
		
		if( j < bcdlen)
			c2 = bcdstr[j] - '0';
		else
			c2 = 0xf; 

		c2 <<=4;
		
		j++;
		
		out[i] = c1|c2;
	}
}


/*
	建立一个空的pcap文件，该文件的作用是pcap_dump_open需要
*/
void create_a_pcap()
{
	unsigned char hexpcapfile[] = {
		0xd4,0xc3,0xb2,0xa1,0x02,0x00,0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0xff,0xff,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0xb8,0x01,0x00,0x00,0xb8,0x01,0x00,0x00,0x5a,0x5a,0x5a,0x5a,0x5a,0x5a,0x6b,0x6b,
		0x6b,0x6b,0x6b,0x6b,0x08,0x00,0x45,0x51,0x01,0x82,0x00,0x05,0x00,0x00,0x1b,0x29,
		0x00,0x00,0xaa,0xaa,0xbb,0xbb,0xcc,0xcc,0xdd,0xdd,0x6f,0x1c,0xbd,0xb4,0x01,0x6e,
		0x06,0x4e,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x12,0x34,0x56,0x78,0x9a,0xbc,
		0xde,0xf0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xaa,0xaa,0xbb,0xbb,0xcc,0xcc,
		0xdd,0xdd,0x12,0x34,0x56,0x78,0x00,0x00,0x00,0x05,0x00,0x00,0x00,0x05,0x50,0x3f,
		0xcd,0xff,0x00,0x00,0xb5,0xe8,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x10,
		0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x20,0x21,0x22,0x23,0x4b,0x12,0x37,
		0xae,0xf8,0x20,0xda,0x22,0xec,0x2a,0x7a,0x1e,0xe9,0xeb,0x0b,0x6e,0x5e,0x7c,0x45,
		0x8b,0x12,0x37,0x83,0xe1,0xce,0x4a,0xd5,0xcd,0xe4,0xf4,0xec,0x1a,0xe3,0xec,0xf3,
		0x2d,0x5c,0xdd,0xec,0x8f,0xff,0x78,0xab,0x27,0xfb,0x13,0x0c,0xbb,0xe6,0x49,0x6a,
		0x0a,0x18,0x96,0x09,0x2e,0xbd,0x66,0x76,0x75,0x8f,0x95,0x7b,0x7c,0x6f,0xc9,0xc9,
		0xb4,0xda,0xad,0xdf,0x0d,0x70,0x83,0x51,0x04,0x80,0xf9,0x06,0xca,0xdf,0xc9,0x84,
		0xb0,0xe9,0x10,0xde,0xac,0x09,0xd7,0x61,0x0d,0x91,0xd3,0x9a,0x2f,0x4e,0xb7,0xc6,
		0x5d,0x9c,0x2f,0x44,0x9b,0x62,0xe9,0x2e,0x8d,0xc6,0xaa,0xa0,0x37,0xf0,0xe8,0xcd,
		0xd1,0xbb,0xdf,0x7d,0x58,0x9f,0x97,0x06,0x29,0xc4,0xd7,0x5d,0x4d,0x73,0x7f,0x4e,
		0xb9,0xe0,0x39,0x81,0x31,0x8f,0xfe,0x5d,0x0f,0x33,0x69,0x56,0x9a,0x5c,0x49,0xd3,
		0x3a,0xd8,0x78,0x39,0x25,0x09,0x56,0x2c,0xd3,0x1a,0x00,0xac,0xe8,0x6c,0xa0,0x1d,
		0xd1,0x00,0xda,0xd8,0xc0,0x4c,0xd1,0x51,0x51,0x45,0xb3,0x7e,0x80,0xfc,0x48,0x81,
		0x31,0xa7,0x83,0x3f,0x00,0x64,0x7e,0xf1,0x8e,0x9e,0xe8,0x49,0x0a,0x5f,0x51,0x49,
		0x25,0x6c,0x58,0x5e,0x2f,0x83,0x27,0xd4,0x94,0x92,0x3c,0x46,0x6f,0x41,0xf7,0x16,
		0x72,0xa2,0xe3,0x92,0xcb,0x68,0x31,0xcb,0x55,0x71,0x5e,0xcd,0xb5,0x08,0x82,0x3f,
		0xb0,0xac,0x32,0x05,0x5e,0xb8,0x7d,0x0a,0x8d,0xc9,0xf1,0xb4,0xe5,0x34,0x23,0x2d,
		0x31,0x5e,0xb4,0x0e,0x64,0x64,0x48,0x8a,0x9b,0xcd,0x6b,0xab,0xe3,0xbd,0xd9,0xc2,
		0xe0,0x5f,0x1d,0x93,0x26,0x05,0x09,0x6c,0x6b,0xb0,0xf7,0xa5,0x57,0x76,0x4f,0xb4,
		0x1d,0x87,0x46,0x66,0x9e,0x3f,0x54,0x54,0x4c,0x06,0x50,0x2d,0x86,0x6a,0xb8,0xef,
		0xa0,0x3f,0x09,0xa8,0x55,0x1e,0xb8,0xce,0xd6,0x25,0xa9,0xcf,0x35,0x40,0xb6,0xf5,
		0x5a,0xe1,0x25,0x27,0x44,0x79,0x9f,0xa8,0xc9,0x85,0x85,0x73,0x88,0x96,0x34,0x3d,
	};
	FILE * fp;

	fp = fopen("org.pcap","w+b");
	if(fp == NULL)
	{
		printf("create pcap file error\n");
		return;
	}

	fwrite(hexpcapfile,1,sizeof(hexpcapfile),fp);
	fclose(fp);
}

pcap_dumper_t* dumpfile_open(char * file)
{
	pcap_t *p;
	char ebuff[1024];

	if(access(file,R_OK) != 0)
	{
		create_a_pcap();
	}
	
    p = pcap_open_offline("org.pcap",ebuff);

	return pcap_dump_open(p, file);
}

void dumpfile_close(pcap_dumper_t * pdump)
{
	pcap_dump_close(pdump);
}

/*
 将字符串根据分隔符进行分隔，得到一系列的字符串指针，将其放到out中返回
 返回的out用完后需要释放，返回值表示得到的串的个数
*/
int split_string( char * str, const char *split, char *out[])
{
	char * ret = strtok(str, split);
	int n = 0;
	
	while(ret != NULL)
	{
		out[n] = strdup(ret);
		n ++;
		ret = strtok(NULL, split);
	}

	return n;
}

void free_split_string(int n, char *out[])
{
	int i;
	for(i = 0 ; i < n ; i++)
		free(out[i]);
}

void uint_step_add(char * buf, char * from, char * to)
{
	uint64_t v;
	uint64_t f,t;
	
	v = atol(buf);
	v ++;

	f = atol(from);
	t = atol(to);

	if(v > t)
		v = f;
	
	sprintf(buf,"%lld",(unsigned long long)v);
}

void ipv4_step_add(char * buf, char * from, char * to)
{
	uint32_t v;
	uint32_t f,t;
	
	v = ntohl(ipv4_string_2_int32(buf));
	f = ntohl(ipv4_string_2_int32(from));
	t = ntohl(ipv4_string_2_int32(to));

	v ++;

	if(v > t)
		v = f;

	v = htonl(v);

	
	struct in_addr network;
	network.s_addr = v;

	strcpy(buf,inet_ntoa(network));
}



