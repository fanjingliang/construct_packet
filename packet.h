
#pragma once

#include "list.h"
#include "define.h"
#include "layer.h"

/*
 描述层的运行时状态，主要是描述

 层中可选字段的显示与否情况
 层中各字段的内容
 层的总内存块的信息

 为拷贝和赋值提供信息支撑
*/


typedef enum
{
	PAYLOAD_TYPE_A_Z,				//a-z递增
	PAYLOAD_TYPE_RANDOM,			//随机值
	PAYLOAD_TYPE_SET,				//用户手工设置值
}PAYLYAD_TYPE;


typedef struct field_running
{
	list_node_t list;

	field_t * field_info;		//字段对应的描述结构的指针

	int	value_bit_len;						//字段的长度, 为了便于计算，这里用位表示

	unsigned char value_tmp[MAX_VALUE_LEN];		//这里是用户赋值命令的值的临时保存位置，这里存的都是明文的数字

	char value_from_to[MAX_VALUE_LEN];
	
} field_running_t;

typedef struct layer_running
{
	list_node_t list;

	layer_t * layer_info;

	//所属的包的指针，所有的层包括子层都指向同一个包
	packet_running_t * packet;
	
	unsigned char buffer[4096];	//层的缓冲区，所有的变量设置完毕后，更新这个缓冲区的内容
								//层拷贝的时候也是用的这个成员。

	int buffer_bit_len;		//层的缓冲区的长度,这里的长度也是bit长度
	
	list_node_t field_list;

	/*
	层中可以加入子层，加入的子层的位置放到原有字段的后面
	子层也可以有子层，也就是可以是一个多层的关系。

	层的子层在关系上相当于当前层的字段
	*/
	list_node_t child_list;

	//为了便于回溯，这里保存父亲指针
	struct layer_running * parent;

	//为了在write命令时的便利，这里增加一个变量表示layername[N], 这里的值是N
	int N;
} layer_running_t;

typedef struct packet_running
{
	list_node_t list;

	char name[MAX_NAME_LEN];

	int len;					//包的总长度, 默认512
	int packet_num;			//本流需要发送的包数量
	int payload_type;			
	char payload_content[2048];
	
	list_node_t layer_list;

	layer_running_t * current_layer;
} packet_running_t ;

void running_init();
list_node_t * running_get_packet_list_head();

packet_running_t* running_create_packet(const char * name);
field_running_t* running_create_field(field_t * pfield);
layer_running_t * running_create_layer(layer_t * play);
field_running_t * running_location_field(layer_running_t* current_layer, const char * name);
packet_running_t* running_find_packet(const char * name);
layer_running_t* running_add_packet_layer(packet_running_t* ppkt, const char * typename);
layer_running_t* running_add_layer_child(layer_running_t * parent, const char * typename);
layer_running_t* running_add_layer_next(layer_running_t * prev, const char * typename);

void running_connstruct_field_list(list_node_t * head, unsigned char * outbuf, int * outlen);
void running_connstruct_child_list(list_node_t * head, unsigned char * outbuf, int * outlen);
void running_construct_layer_checksum(layer_running_t * play);
void running_construct_layer(layer_running_t * play);
void running_construct_payload(packet_running_t * pkt,unsigned char * buf, int len);
void running_construct_packet(packet_running_t * pkt, unsigned char * out_buf, int * out_len);
void running_compute_field(field_running_t* pfield);
void running_compute_layer(layer_running_t * play);
void running_set_layer_len_field(layer_running_t * play, int payload_len);
void running_compute_packet(packet_running_t * pkt);
int running_check_max_packet(packet_running_t * pkt);
void running_step_packet(packet_running_t * pkt);


