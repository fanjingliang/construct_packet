
#pragma once

#include <unistd.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>  
#include <pcap.h>

unsigned short checksum(unsigned short *buf, int nword);
uint64_t hton64(uint64_t q);

#define ntoh64 hton64


uint32_t ipv4_string_2_int32(const char * ipv4str);
int inet_pton4(const char *src, unsigned char *dst);
int inet_pton6(const char *src, unsigned char *dst);
void ipv6_string_2_uint64(const char *src, unsigned char *dst);
void bcd_string_2_buf(const char * bcdstr, char *out, int len);
void hexstring_2_buf(const unsigned char * hexstring, unsigned char *outbuf, int len);
void hexstring_2_buf_2(const unsigned char * hexstring, unsigned char *outbuf, int *len);
pcap_dumper_t* dumpfile_open(char * file);
void dumpfile_close(pcap_dumper_t * pdump);
int split_string( char * str, const char *split, char *out[]);
void ipv4_step_add(char * buf, char * from, char * to);
void uint_step_add(char * buf, char * from, char * to);
void free_split_string(int n, char *out[]);



