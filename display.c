
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "script.h"
/*
---------------------------------------------------------------------
flow: xxxxxx	|
  eth			|
  ip			|
  ip <...>		|
  ip			|
  tcp			|
  tcp			|
  	abc			|
  	abc <...>	|
  	[xyz] <...>	|
  	exy  		|	
  udp			|
*/

static char head_info[1024][1024];
static int head_info_line_num;
static size_t head_info_max_len;

static char tail_info[1024][1024];
static int tail_info_line_num;
static int result_print = 1;

void display_init()
{
	if(result_print == 0)	return;
		
	memset(head_info,0,sizeof(head_info));
	memset(tail_info,0,sizeof(tail_info));
	head_info_line_num = 0;
	head_info_max_len = 0;
	tail_info_line_num = 0;
}

void head_info_printf_line(const char *fmt, ...)
{
	if(result_print == 0)	return;

	if(head_info_line_num>= 1024) return;
	
	va_list args;
	va_start (args, fmt);
	vsnprintf(head_info[head_info_line_num],1023,fmt,args);
	va_end(args);

	if(strlen(head_info[head_info_line_num]) > head_info_max_len)
	{
		head_info_max_len = strlen(head_info[head_info_line_num]);
	}
	head_info_line_num++;
}


void head_info_update()
{
	cmd_info_current_flow_struct_simple();
}

void tail_info_printf_line(const char *fmt, ...)
{
	if(result_print == 0)	return;
	if(tail_info_line_num >= 1024) return;
	
	va_list args;
	va_start (args, fmt);
	vsnprintf(tail_info[tail_info_line_num],1023,fmt,args);
	va_end(args);

	tail_info_line_num++;
}

void display_content()
{
	if(result_print == 0)	return;
	
	int maxnum = head_info_line_num>tail_info_line_num? head_info_line_num:tail_info_line_num ;

	int i;
	printf("--------------------------------------------------------------------------------------------\n");


	char fmt[128] = {0};
	sprintf(fmt,"%%-%us | %%s\n",head_info_max_len);

	for(i = 0 ; i < maxnum ; i++)
	{
		printf(fmt,head_info[i],tail_info[i]);
	}
}

void display_set(int need_print_screen)
{
	result_print = need_print_screen;
}

