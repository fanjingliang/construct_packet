
#pragma once

#include <stdint.h>
#include "list.h"
#include "define.h"

typedef enum
{
	field_fixed,
	field_optional,
} field_optional_type;

typedef enum
{
	value_type_default,
//	value_type_random,
	value_type_checksum,
	value_type_len_include_head_len,
	value_type_len_notinclude_head_len,
	value_type_set,
} value_type;

typedef enum
{
	value_data_type_uint8,
	value_data_type_uint16,
	value_data_type_uint32,
	value_data_type_uint64,
	value_data_type_ipv4,
	value_data_type_ipv6,
	value_data_type_bcd,
	value_data_type_hexstring, //example, 0x12abcdef1233eeff
	value_data_type_hexvar,
} value_data_type;

typedef struct field
{
	list_node_t list;

	list_node_t list_tmp;	//用于读取配置文件时，临时使用
	
	//字段名称
	char name[MAX_NAME_LEN];

	//字段长度
	int  len;
	int  bitlen;

	//值类型
	value_type vtype;
	value_data_type dtype;
	
	unsigned char default_value[MAX_VALUE_LEN];
}field_t;

typedef struct layer
{
	list_node_t list;

	//该层是属于哪个库中，以后需要可以增加4G库，3G库等
	char library[128];
	
	//层名称
	char name[MAX_NAME_LEN];

	//层描述信息
	char description[4096];
	
	//层中的字段的链表
	list_node_t field_list;
}layer_t;

void init_layers();
layer_t * create_layer(const char * name);
field_t * create_field(const char * name,  int otype, int len, int bitlen, int vtype, int dtype,const char *def_value);
void add_field(layer_t * parent, field_t * child);
void add_layer( layer_t * new_layer);
layer_t * location_layer( const char * name);
field_t * location_field_base( list_node_t * head, const char * name);
field_t * location_field( layer_t * play, const char * name);



