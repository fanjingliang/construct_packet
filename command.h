

#pragma once

#include <stdio.h>

enum
{
	CMD_RET_OK,
	CMD_RET_ERR,
	CMD_RET_QUIT,
}CMD_FUNCTION_RETURN;

typedef int  (* CMD_FUNCTION) ( int args , char* argv[]);
typedef void (* CMD_HELP) (char **name, char ** param_list, char **brief_desc, char **detailed_desc);

typedef struct cmd_node
{
	char *name;
	char *param_list;
	char *brief_desc;
	char *detailed_desc;
	
	CMD_FUNCTION func;
	struct cmd_node * next;
}cmd_node_t;




void do_reg_commands();

void do_scanf_stdin();
void do_scanf_file(const char * file);
void do_scanf_buf(char *buf);


