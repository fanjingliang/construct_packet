
APP_NAME = packet_build


SRCDIR =.

SRCS := $(shell ls *.c )
SRCS := $(notdir $(SRCS))
OBJS := $(SRCS:.c=.o)
OBJS := $(addprefix obj/,$(SRCS:.c=.o))


INCLUDE_PATH = -I $(SRCDIR)

CFLAGS_LOCAL += -g -W -Wall -Wno-unused-parameter $(INCLUDE_PATH)



.PHONY: all

all: objpath $(APP_NAME)

objpath:
	if [ ! -d obj ]; then mkdir obj; fi

$(OBJS): obj/%.o : %.c
	gcc $(CFLAGS_LOCAL) -c -o $@ $<

$(APP_NAME): $(OBJS)
	gcc $(CFLAGS_LOCAL) -o $@ $^ -lm -lpcap -lreadline

clean:
	
	rm -f $(APP_NAME)
	rm -fr obj

