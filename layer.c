
#include "layer.h"
#include <stdlib.h>
#include <string.h>
#include "global.h"

static list_node_t g_layers;

void init_layers()
{
	INIT_LIST_HEAD(&g_layers);
}

layer_t * create_layer(const char * name)
{
	layer_t * p = (layer_t*)malloc(sizeof(layer_t));

	memset(p,0,sizeof(layer_t));

	strcpy(p->name , name);

	INIT_LIST_HEAD(&p->field_list);
	return p;
}

field_t * create_field(const char * name,  int otype, int len, int bitlen, int vtype, int dtype, const char *def_value)
{
	field_t * p = (field_t*)malloc(sizeof(field_t));

	memset(p,0,sizeof(field_t));

	strcpy(p->name , name);

	p->len = len;
	p->bitlen = bitlen;
	p->vtype = vtype;
	p->dtype = dtype;
	if(vtype == value_type_default)
	{
		strcpy((char *)p->default_value,def_value);
	}

	return p;
}

void add_field(layer_t * parent, field_t * child)
{
	list_add_tail(&parent->field_list, & child->list);
}

void add_layer( layer_t * new_layer)
{
	list_add_tail(&g_layers, &new_layer->list);
}

layer_t * location_layer( const char * name)
{
	list_node_t *head;
	list_node_t *node;

	head = &g_layers;

	node = head->next;

	while(node != head)
	{
		layer_t *play;

		play = LIST_ENTRY(node,layer_t,list);
		if(strcmp(play->name,name)==0)
		{
			return play;
		}
		node = node->next;
	}
	return NULL;
}


field_t * location_field_base( list_node_t * head, const char * name)
{
	list_node_t *node;

	node = head->next;

	while(node != head)
	{
		field_t * pfield;
		pfield = LIST_ENTRY(node,field_t, list);
		if(strcmp(pfield->name,name) == 0)
		{
			return pfield;
		}
		node = node->next;
	}
	
	return NULL;
}



/*
 这里的参数name表示的是层下面的字段
*/
field_t * location_field( layer_t * play, const char * name)
{
	list_node_t * head;

	head = &play->field_list;

	field_t * pret;

	pret = location_field_base(head,name);
	return pret;
}


