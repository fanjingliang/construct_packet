
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
	

#include "packet.h"
#include <stdlib.h>
#include <string.h>
#include "global.h"
#include "pcap.h"
#include "layer.h"
#include "tool.h"
#include "alias.h"

enum
{
	RUNNING_RET_OK = 0,
	RUNNING_RET_PACKET_EXIST,
	RUNNING_RET_NO_PACKET,
	RUNNING_RET_NO_LAYER,
	RUNNING_RET_NO_FIELD,
};

static list_node_t g_packets;

static int g_field_max_range;

void running_init()
{
	INIT_LIST_HEAD(&g_packets);
	init_layers();
	init_alias();
}

/*
返回当前包链表的头
*/
list_node_t * running_get_packet_list_head()
{
	return &g_packets;
}

/*
返回当前的包的指针，所有的操作都是基于当前包来进行的。
*/
packet_running_t* running_get_current_packet()
{
	list_node_t * head = *g_packets;
	list_node_t * node = head->next;

	if(node != head)
	{
		packet_running_t * ppkt;
		
		ppkt = LIST_ENTRY(node,packet_running_t, list);
		return ppkt;
	}
	else
	{
		return NULL;
	}
}


/*
 建立一个指定名称的包类型，如果同样名字的已经存在，那么返回false
*/
int running_create_packet(const char * name)
{
	if(running_select_packet(name) == RUNNING_RET_OK)
	{
		return RUNNING_RET_PACKET_EXIST;
	}
	
	//没有找到，新建立一个
	packet_running_t * pnew = (packet_running_t *)malloc(sizeof(packet_running_t ));
	
	strcpy(pnew->name, name);
	pnew->len = 10;
	
	INIT_LIST_HEAD(&pnew->layer_list);

	//将包加入到包链表的头部，就相当于设置为当前包了。
	list_add_head(&g_packets,&pnew->list);
	
	return RUNNING_RET_OK;
}


//根据字段的描述信息，生成一个运行时结构，同时生成它的子结构
field_running_t* __running_create_field(field_t * pfield)
{
	field_running_t * pfieldrun = (field_running_t*) malloc(sizeof(field_running_t));

	memset(pfieldrun,0,sizeof(field_running_t));

	pfieldrun->field_info = pfield;

	//固定字段根据参数设置初始值。
	//这里只设置random,default的即可
	if(pfield->vtype == value_type_default)
	{
		strcpy((char *)pfieldrun->value_from_to, (char *)pfield->default_value);
	}

	return pfieldrun;
}

//根据层的描述信息，生成一个运行时结构
layer_running_t * __running_create_layer(layer_t * play)
{
	layer_running_t* playrun = (layer_running_t*)malloc(sizeof(layer_running_t));

	memset(playrun,0,sizeof(layer_running_t));


	playrun->layer_info = play;

	INIT_LIST_HEAD(&playrun->field_list);
	INIT_LIST_HEAD(&playrun->child_list);

	if(!list_empty(&play->field_list))
	{
		list_node_t * head;
		list_node_t * node;

		head = &play->field_list;
		node = head->next;

		while(node != head)
		{
			field_t * pfield = LIST_ENTRY(node, field_t , list);
			field_running_t *pfieldrun = __running_create_field(pfield);
			list_add_tail(&playrun->field_list,&pfieldrun->list);

			node = node->next;
		}
	}

	return playrun;
}

int running_set_field_value(const char * name , const char * value)
{
	packet_running_t * ppkt = running_get_current_packet();

	if(ppkt == NULL) 
		return RUNNING_RET_NO_PACKET;

	if(ppkt->current_layer == NULL)
		return RUNNING_RET_NO_LAYER;
		
	list_node_t * head;
	list_node_t * node;
	
	head = &ppkt->current_layer->field_list;
	node = head->next;
	
	while(node != head)
	{
		field_running_t * pfield = NULL;

		pfield = LIST_ENTRY(node,field_running_t, list);
		
		if(strcmp(pfield->field_info->name,name) == 0)
		{
			strcpy(pfield->value_from_to, value);
			return RUNNING_RET_OK;
		}
		node = node->next;
	}

	return RUNNING_RET_NO_FIELD;

}

packet_running_t* running_pop_packet(const char * name)
{
	list_node_t *head;
	list_node_t *node;

	head = &g_packets;
	node = head->next;

	while(node != head)
	{
		packet_running_t * ppkt;
		
		ppkt = LIST_ENTRY(node,packet_running_t, list);

		if(strcmp(ppkt->name,name) == 0)
		{
			list_del_node( &ppkt->list) ;
			return ppkt;
		}
		node = node->next;
	}
	return NULL;
}

void running_delete_layer(layer_running_t * play)
{
	//删除自己的所有字段

	list_node_t *head;
	list_node_t *node;

	head = &play->field_list;

	field_running_t * pfield;

	while(!list_empty(head))
	{
		node = head->next;
		pfield = LIST_ENTRY(node,field_running_t, list);
		list_del_node(&pfield->list);
		free(pfield);
	}

	//删除自己的所有子层

	head = &play->child_list;

	layer_running_t * pchildlay;

	while(!list_empty(head))
	{
		node = head->next;
		pchildlay = LIST_ENTRY(node,layer_running_t, list);
		list_del_node(&pchildlay->list);
		running_delete_layer(pchildlay);
	}

	//如果删除的是当前层那么将其设置为空.
	if(play == play->packet->current_layer)
	{
		play->packet->current_layer = NULL;
	}
	//删除自己
	free(play);
}

void running_delete_packet(packet_running_t * ppkt)
{
	list_node_t *head;
	list_node_t *node;

	head = &ppkt->layer_list;

	layer_running_t * play;

	while(!list_empty(head))
	{
		node = head->next;
		play = LIST_ENTRY(node,layer_running_t, list);
		list_del_node(&play->list);
		running_delete_layer(play);
	}
}

/*
 将其选择为当前的包,就是将其放到链表的头部
*/
bool running_select_packet(const char * name)
{
	list_node_t *head;
	list_node_t *node;

	head = &g_packets;
	node = head->next;

	while(node != head)
	{
		packet_running_t * ppkt;
		
		ppkt = LIST_ENTRY(node,packet_running_t, list);

		if(strcmp(ppkt->name,name) == 0)
		{
			list_del_node(&ppkt->list);
			list_add_head(&g_packets,&ppkt->list);
			return true;
		}
		node = node->next;
	}
	return false;
}

void running_set_layer_belong_to(layer_running_t *play, packet_running_t* ppkt)
{
	list_node_t *head;
	list_node_t *node;

	head = &play->child_list;
	node = head->next;
	
	layer_running_t * pchildlay;

	while(node != head)
	{
		pchildlay = LIST_ENTRY(node,layer_running_t, list);
		running_set_layer_belong_to(pchildlay,ppkt);
		node = node->next;
	}
	play->packet = ppkt;
}

layer_running_t*  running_add_packet_layer(packet_running_t* ppkt, const char * typename)
{
	if(ppkt == NULL) return NULL;

	layer_running_t * play;

	layer_t * p = location_layer(typename);

	if(p == NULL) return NULL;

	play = running_create_layer(p);
	running_set_layer_belong_to(play , ppkt);
	
	list_add_tail(&ppkt->layer_list, &play->list);

	ppkt->current_layer = play;

	return play;	
}

layer_running_t* running_add_layer_next(layer_running_t * prev, const char * typename)
{
	if(prev == NULL) return NULL;

	
	layer_running_t * play;

	layer_t * p = location_layer(typename);

	if(p == NULL) return NULL;

	play = running_create_layer(p);
	running_set_layer_belong_to(play,prev->packet);

	list_add_head(&prev->list, &play->list);

	play->parent = prev->parent;

	play->packet->current_layer = play;
	
	return play;
}


layer_running_t* running_add_layer_child(layer_running_t * parent, const char * typename)
{
	if(parent == NULL) return NULL;

	
	layer_running_t * play;

	layer_t * p = location_layer(typename);

	if(p == NULL) return NULL;

	play = running_create_layer(p);
	running_set_layer_belong_to(play,parent->packet);

	list_add_tail(&parent->child_list, &play->list);

	play->parent = parent;

	play->packet->current_layer = play;
	return play;
}

uint8_t running_bit_set_value8(uint8_t old, int bitindex, int bitlen, int value)
{
	uint8_t tmp;
	uint8_t andv = (0xff << (8-bitlen)) >> (8-bitlen);

	tmp = value;
	tmp = tmp & andv;

	return tmp << (8-(bitindex+bitlen)) ;	
}

uint16_t running_bit_set_value16(uint16_t old, int bitindex, int bitlen, int value)
{
	uint16_t tmp;
	uint16_t andv = (0xffff << (16-bitlen)) >> (16-bitlen);

	tmp = value;
	tmp = tmp & andv;

	return tmp << (16-(bitindex+bitlen)) ;	
}

uint32_t running_bit_set_value32(uint32_t old, int bitindex, int bitlen, int value)
{
	uint32_t tmp;
	uint32_t andv = (0xffffffff << (32-bitlen)) >> (32-bitlen);

	tmp = value;
	tmp = tmp & andv;

	return tmp << (32-(bitindex+bitlen)) ;	
}

uint64_t running_bit_set_value64(uint64_t old, int bitindex, int bitlen, int value)
{
	uint64_t tmp;
	uint64_t andv = (0xffffffffffffffff << (64-bitlen)) >> (64-bitlen);

	tmp = value;
	tmp = tmp & andv;

	return tmp << (64-(bitindex+bitlen)) ;	
}



//将列表中所有的field_running_t结构的值组合到一起
void running_connstruct_field_list(list_node_t * head, unsigned char * outbuf, int * outlen)
{
	int pos;			//当前写入的位置

	int bit_num;		//当前位操作已经进行了多少位，
						//该值用于表示某个字段的位索引
	uint8_t u8 = 0;
	uint16_t u16 = 0;
	uint32_t u32 = 0;
	uint64_t u64 = 0;

	
	pos = 0;
	bit_num = 0;

	list_node_t *node;
	node = head->next;
	while(node != head)
	{
		field_running_t * pfieldrun ;

		pfieldrun = LIST_ENTRY(node , field_running_t, list);
		field_t *pfield = pfieldrun->field_info;
		if(pfield->bitlen > 0)
		{
			int value = atoi((char *)pfieldrun->value_tmp);
			
			switch(pfield->len)
			{
				case 1:
					u8 |= running_bit_set_value8(u8, bit_num,  pfield->bitlen,value);
					break;
				case 2:
					u16 |= running_bit_set_value16(u16, bit_num,  pfield->bitlen,value);
					break;
				case 4:
					u32 |= running_bit_set_value32(u32, bit_num,  pfield->bitlen,value);
					break;
				case 8:
					u64 |= running_bit_set_value64(u64, bit_num,  pfield->bitlen,value);
					break;
			}
			bit_num += pfield->bitlen;

			//如果位和等于了总位数，也要进行重置bit_num
			if(bit_num == pfield->len * 8)
			{
				//先更新值内容
				switch(pfield->len)
				{
					case 1:
						outbuf[pos] = u8;
						pos ++;
						break;
					case 2:
						u16 = htons(u16);
						memcpy(&outbuf[pos],&u16,2);
						pos += 2;
						break;
					case 4:
						u32 = htonl(u32);
						memcpy(&outbuf[pos],&u32,4);
						pos +=4;
						break;
					case 8:
						u64 = hton64(u64);
						memcpy(&outbuf[pos],&u64,8);
						pos += 8;
						break;
				}
				
				bit_num = 0;
				u8 = 0;
				u16 = 0;
				u32 = 0;
				u64 = 0;
			}
		}
		else
		{
			field_t *pfield = pfieldrun->field_info;
			uint64_t v = 0;
			uint8_t uv8 = 0;
			uint16_t uv16 = 0;
			uint32_t uv32 = 0;
			uint64_t uv64 = 0;
			v = atol((char *)pfieldrun->value_tmp);
			
			switch(pfield->dtype)
			{
				case value_data_type_uint8:
					uv8 = v;
					memcpy(&outbuf[pos], &uv8, 1);
					break;
				case value_data_type_uint16:
					uv16 = v;
					uv16 = htons(uv16);
					memcpy(&outbuf[pos], &uv16, 2);
					break;
				case value_data_type_uint32:
					uv32 = v;
					uv32 = htonl(uv32);
					memcpy(&outbuf[pos], &uv32, 4);
					break;
				case value_data_type_uint64:
					uv64 = hton64(v);
					memcpy(&outbuf[pos], &uv64, 8);
					break;
				case value_data_type_ipv4:
					uv32 = ipv4_string_2_int32((char *)pfieldrun->value_tmp);
					memcpy(&outbuf[pos], &uv32, 4);
					break;
				case value_data_type_ipv6:
				{
					ipv6_string_2_uint64((char *)pfieldrun->value_tmp, &outbuf[pos]);
				}
					break;
				case value_data_type_bcd:
					bcd_string_2_buf((const char *)pfieldrun->value_tmp,(char *)&outbuf[pos],pfield->len);
					break;
				case value_data_type_hexstring:
					hexstring_2_buf((const unsigned char *)pfieldrun->value_tmp, &outbuf[pos], pfield->len);
					break;
				case value_data_type_hexvar:
					pfield->len = strlen((char *)pfieldrun->value_tmp)/2;
					hexstring_2_buf((const unsigned char *)pfieldrun->value_tmp, &outbuf[pos], pfield->len);
					break;
			}

			pos += pfield->len;
		}
		node = node->next;
	}	

	*outlen = pos*8;
}

//将列表中所有的子层的内容组合到一起
void running_connstruct_child_list(list_node_t * head, unsigned char * outbuf, int * outlen)
{
	int pos;			//当前写入的位置
	int bit_num;		//当前位操作已经进行了多少位，

	pos = 0;
	bit_num = 0;

	list_node_t *node;
	node = head->next;
	while(node != head)
	{
		layer_running_t * player ;

		player = LIST_ENTRY(node , layer_running_t, list);

		running_construct_layer(player);

		bit_num += player->buffer_bit_len;
		memcpy(&outbuf[pos], player->buffer, player->buffer_bit_len/8);
		pos += player->buffer_bit_len/8;
		
		node = node->next;
	}	

	*outlen = bit_num;
}


void running_construct_layer_checksum(layer_running_t * play)
{
	list_node_t *head;
	list_node_t *node;

	head = &play->field_list;
	node = head->next;

	int bit_pos = 0;

	while(node != head)
	{
		field_running_t * pfieldrun;
		pfieldrun = LIST_ENTRY(node , field_running_t, list);

		if(pfieldrun->field_info->vtype == value_type_checksum)
		{
			goto checksum_flag;
		}
		else if(pfieldrun->field_info->bitlen > 0)
		{
			bit_pos += pfieldrun->field_info->bitlen;
		}
		else
		{
			bit_pos += pfieldrun->field_info->len * 8;
		}

		node = node->next;
	}

	return;
	
checksum_flag:
	;
	//此地需要开始做校验和
	int pos;
	pos = bit_pos/8;

	play->buffer[pos] = 0;
	play->buffer[pos+1] = 0;

	unsigned short csum = checksum((unsigned short *)play->buffer,play->buffer_bit_len/8);
	*(unsigned short*)&(play->buffer[pos]) = csum;
	
}

void running_construct_layer(layer_running_t * play)
{
	int bitlen1 = 0;
	int bitlen2 = 0;

	running_connstruct_field_list(&play->field_list, play->buffer, &bitlen1);
	running_construct_layer_checksum(play);

	running_connstruct_child_list(&play->child_list,&play->buffer[bitlen1/8], &bitlen2);

	play->buffer_bit_len = bitlen1 + bitlen2;

}

void running_construct_payload(packet_running_t * pkt,unsigned char * buf, int len)
{
	unsigned char alpha[] = {
		'a','b','c','d','e','f','g','h','i','j','k','l','m',
		'n','o','p','q','r','s','t','u','v','w','x','y','z'
		};
	if(pkt->payload_type == 0) /*a-z递增*/
	{
		int i = 0;
		for(i = 0 ; i < len ; i++)
		{
			buf[i] = alpha[i%26];
		}
	}
	else if(pkt->payload_type == 1) /*random*/
	{
		int i = 0;
		for(i = 0 ; i < len ; i++)
		{
			buf[i] = random();
		}
	}
	else /*手工配置的负载内容*/
	{
		memcpy(buf,pkt->payload_content,len);
	}
}


void running_construct_packet(packet_running_t * pkt, unsigned char * out_buf, int * out_len)
{
	list_node_t *head;
	list_node_t *node;

	head = &pkt->layer_list;
	node = head->next;

	int bufferlen = 0;
	
	while(node != head)
	{
		layer_running_t * playerrun;
		playerrun = LIST_ENTRY(node , layer_running_t, list);

		running_construct_layer(playerrun);

		memcpy(&out_buf[bufferlen],playerrun->buffer,playerrun->buffer_bit_len/8);

		bufferlen += playerrun->buffer_bit_len/8;

		//display_buffer(playerrun->layer_info->name, playerrun->buffer,playerrun->buffer_bit_len/8);
		
		node = node->next;
	}

	if(pkt->len > bufferlen)
	{
		running_construct_payload(pkt,&out_buf[bufferlen],pkt->len - bufferlen);
		//display_buffer("payload",&out_buf[bufferlen],pkt->len - bufferlen);
		*out_len = pkt->len;
	}
	else
	{
		*out_len = bufferlen;
	}
}



void running_compute_field(field_running_t* pfield)
{
	field_t * pf = pfield->field_info;

	if(pf->bitlen > 0)
	{
		pfield->value_bit_len = pf->bitlen;
	}
	else
	{
		if(pf->dtype == value_data_type_hexvar)
		{
			pfield->value_bit_len = (strlen((char *)pfield->value_tmp)/2) * 8;
		}
		else
		{
			pfield->value_bit_len = pf->len * 8;
		}
	}
}


void running_compute_layer(layer_running_t * play)
{
	list_node_t *head;
	list_node_t *node;

	head = &play->field_list;
	node = head->next;

	int len = 0;

	//先将所有字段的长度加和
	while(node != head)
	{
		field_running_t * pfieldrun;
		pfieldrun = LIST_ENTRY(node , field_running_t, list);

		running_compute_field(pfieldrun);
		
		len += pfieldrun->value_bit_len;
		
		node = node->next;
	}

	//子层的长度也要计算上去
	head = &play->child_list;
	node = head->next;
	
	while(node != head)
	{
		layer_running_t * pplayrun;
		pplayrun = LIST_ENTRY(node , layer_running_t, list);

		running_compute_layer(pplayrun);

		len += pplayrun->buffer_bit_len;
		
		node = node->next;
	}
	play->buffer_bit_len = len;
	
}

//每层只有一个长度字段
void running_set_layer_len_field(layer_running_t * play, int payload_len)
{
	
	list_node_t *head;
	list_node_t *node;

	head = &play->field_list;
	node = head->next;

	while(node != head)
	{
		field_running_t * pfieldrun;

		pfieldrun = LIST_ENTRY(node,field_running_t,list);

		if(pfieldrun->field_info->vtype == value_type_len_include_head_len)
		{
			
			sprintf((char *)pfieldrun->value_tmp,"%d", payload_len + play->buffer_bit_len/8);
			break;
		}
		else if(pfieldrun->field_info->vtype == value_type_len_notinclude_head_len)
		{
			sprintf((char *)pfieldrun->value_tmp,"%d", payload_len);
			break;
		}
		
		node = node->next;
	}
}

//计算长度值要从上层开始计算，所以要反向遍历
void running_compute_packet(packet_running_t * pkt)
{
	list_node_t *head;
	list_node_t *node;

	head = &pkt->layer_list;
	node = head->prev;

	int all_layer_total_bit_len = 0;
	int all_layer_total_len = 0;
	//先计算各层的长度
	while(node != head)
	{
		layer_running_t * playerrun;
		playerrun = LIST_ENTRY(node , layer_running_t, list);

		running_compute_layer(playerrun);
		all_layer_total_bit_len += playerrun->buffer_bit_len;
		
		node = node->prev;
	}

	all_layer_total_len = all_layer_total_bit_len/8;

	int payload_len;

	if(pkt->len < all_layer_total_len)
	{
		payload_len = 0;
	}
	else
	{
		payload_len = pkt->len - all_layer_total_len;
	}

	//再根据总长度的要求，设置负载的长度，然后配置各层的长度字段
	head = &pkt->layer_list;
	node = head->prev;

	while(node != head)
	{
		layer_running_t * playerrun;
		playerrun = LIST_ENTRY(node , layer_running_t, list);

		
		running_set_layer_len_field(playerrun, payload_len);

		payload_len += playerrun->buffer_bit_len/8;
		node = node->prev;
	}
	
	
}

void running_step_field(field_running_t* pfield)
{
	if(pfield->field_info->dtype == value_data_type_uint8 ||
		pfield->field_info->dtype == value_data_type_uint16 ||
		pfield->field_info->dtype == value_data_type_uint32 ||
		pfield->field_info->dtype == value_data_type_uint64)
	{
		char from[128] = {0};
		char to[128] = {0};
		if(strchr(pfield->value_from_to,'-'))
		{
			sscanf(pfield->value_from_to,"%[^-]-%[^-]", from,to);
			if(strlen((char *)pfield->value_tmp) == 0)
			{
				strcpy((char *)pfield->value_tmp,from);
			}
			else
			{
				uint_step_add(pfield->value_tmp, from, to );
			}
		}
		else
		{
			strcpy((char *)pfield->value_tmp,pfield->value_from_to);
		}
	}
	else if(pfield->field_info->dtype == value_data_type_ipv4)
	{
		char from[128] = {0};
		char to[128] = {0};
		if(strchr(pfield->value_from_to,'-'))
		{
			sscanf(pfield->value_from_to,"%[^-]-%[^-]", from,to);
			if(strlen((char *)pfield->value_tmp) == 0)
			{
				strcpy((char *)pfield->value_tmp,from);
			}
			else
			{
				ipv4_step_add(pfield->value_tmp, from, to);
			}
		}
		else
		{
			strcpy(pfield->value_tmp,pfield->value_from_to);
		}
	}
	else
	{
		strcpy(pfield->value_tmp,pfield->value_from_to);
	}
	
}

void running_step_layer(layer_running_t *play)
{
	list_node_t *head;
	list_node_t *node;

	head = &play->field_list;
	node = head->next;

	while(node != head)
	{
		field_running_t * pfieldrun;
		pfieldrun = LIST_ENTRY(node , field_running_t, list);

		running_step_field(pfieldrun);

		node = node->next;
	}

	
	head = &play->child_list;
	node = head->next;

	while(node != head)
	{
		layer_running_t * playrun;
		playrun = LIST_ENTRY(node , layer_running_t, list);

		running_step_layer(playrun);

		node = node->next;
	}
	return ;
}

void running_step_packet(packet_running_t * pkt)
{
	list_node_t *head;
	list_node_t *node;

	head = &pkt->layer_list;
	node = head->next;

	while(node != head)
	{
		layer_running_t * playerrun;
		playerrun = LIST_ENTRY(node , layer_running_t, list);

		running_step_layer(playerrun);
		
		node = node->next;
	}
}

void running_check_max_field(field_running_t* pfield)
{
	pfield->value_tmp[0] = 0;
	int n = 1;

	if(pfield->field_info->dtype == value_data_type_uint8 ||
		pfield->field_info->dtype == value_data_type_uint16 ||
		pfield->field_info->dtype == value_data_type_uint32 ||
		pfield->field_info->dtype == value_data_type_uint64)
	{
		char from[128] = {0};
		char to[128] = {0};
		if(strchr(pfield->value_from_to,'-'))
		{
			sscanf(pfield->value_from_to,"%[^-]-%[^-]", from,to);
			n = atoi(to) - atoi(from) + 1;
		}
	}
	else if(pfield->field_info->dtype == value_data_type_ipv4)
	{
		char from[128] = {0};
		char to[128] = {0};
		if(strchr(pfield->value_from_to,'-'))
		{
			sscanf(pfield->value_from_to,"%[^-]-%[^-]", from,to);
			uint32_t ifrom, ito;
			ifrom = ipv4_string_2_int32(from);
			ito = ipv4_string_2_int32(to);

			n = ntohl(ito) - ntohl(ifrom) + 1;
		}
	}

	if(n > g_field_max_range)
	{
		g_field_max_range = n;
	}
}

void running_check_max_layer(layer_running_t *play)
{
	list_node_t *head;
	list_node_t *node;

	head = &play->field_list;
	node = head->next;

	while(node != head)
	{
		field_running_t * pfieldrun;
		pfieldrun = LIST_ENTRY(node , field_running_t, list);

		running_check_max_field(pfieldrun);

		node = node->next;
	}

	
	head = &play->child_list;
	node = head->next;

	while(node != head)
	{
		layer_running_t * playrun;
		playrun = LIST_ENTRY(node , layer_running_t, list);

		running_check_max_layer(playrun);

		node = node->next;
	}
	return ;
}

int running_check_max_packet(packet_running_t * pkt)
{
	list_node_t *head;
	list_node_t *node;

	head = &pkt->layer_list;
	node = head->next;

	g_field_max_range = 1;
	
	while(node != head)
	{
		layer_running_t * playerrun;
		playerrun = LIST_ENTRY(node , layer_running_t, list);

		running_check_max_layer(playerrun);
		
		node = node->next;
	}

	return g_field_max_range;
}


