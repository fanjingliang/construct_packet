
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>


#include <fcntl.h>
#include <sys/utsname.h>
#include <net/if.h>
#include <netinet/in.h>
#include <linux/if_ether.h>
#include <net/if_arp.h>
#include <netpacket/packet.h>
#include <stdio.h>
#include <unistd.h>


static inline size_t strlcpy(char *dst, const char *src, size_t siz)
{
	register char *d = dst;
	register const char *s = src;
	register size_t n = siz;

	/* Copy as many bytes as will fit */
	if (n != 0 && --n != 0)
	{
		do
		{
			if ((*d++ = *s++) == 0)
				break;
		} while (--n != 0);
	}

	/* Not enough room in dst, add NUL and traverse rest of src */
	if (n == 0)
	{
		if (siz != 0)
			*d = '\0';		/* NUL-terminate dst */
		while (*s++)
			;
	}

	return(s - src - 1);	/* count does not include NUL */
}

static int get_iface_index(int fd, const int8_t *device) 
{
	struct ifreq ifr;

	memset(&ifr, 0, sizeof(ifr));
	strlcpy(ifr.ifr_name, (const char *)device, sizeof(ifr.ifr_name));

	if (ioctl(fd, SIOCGIFINDEX, &ifr) == -1)
	{
		return (-1);
	}

	return ifr.ifr_ifindex;
}              

int my_socket_init(char *device)
{
	int mysocket;

	if ((mysocket = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) 
	{
		printf("socket: %s\n", strerror(errno));
		return 0;
	}

	struct sockaddr_ll sa;
	 if ((sa.sll_ifindex = get_iface_index(mysocket, device)) < 0)
	 {
        close(mysocket);
        return 0; 
    }

	/* bind socket to our interface id */
	
	
	sa.sll_family = AF_PACKET;
	sa.sll_protocol = htons(ETH_P_ALL);
	if (bind(mysocket, (struct sockaddr *)&sa, sizeof(sa)) < 0)
	{
		printf("bind error: %s\n", strerror(errno));
		close(mysocket);
		return 0;
	}
    
    /* check for errors, network down, etc... */
    int err;
    socklen_t errlen = sizeof(err);
    if (getsockopt(mysocket, SOL_SOCKET, SO_ERROR, &err, &errlen) < 0)
    {
        printf("error opening %s: %s\n", device, strerror(errno));
        close(mysocket);
        return 0;
    }
    
    if (err > 0) 
    {
        printf("error opening %s: %s\n", device, strerror(err));
        close(mysocket);
        return 0;
    }

    /* get hardware type for our interface */
     struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    strlcpy(ifr.ifr_name, device, sizeof(ifr.ifr_name));
    
    if (ioctl(mysocket, SIOCGIFHWADDR, &ifr) < 0)
    {
        close(mysocket);
        printf("Error getting hardware type: %s\n", strerror(errno));
        return 0;
    }

	/* make sure it's not loopback (PF_PACKET doesn't support it) */
	if (ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER)
		printf("Unsupported physical layer type 0x%04x on %s.  Maybe it works, maybe it wont."
		"  See tickets #123/318\n", ifr.ifr_hwaddr.sa_family, device);

	return mysocket;
}

int my_socket_send(int id, void* buf, int len)
{
	int i = 5;

	while(i > 0 )
	{
		if(send(id,buf,len,0) >= 0)
			break;
		i--;
	}

	return 0;
}

