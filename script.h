
#include <string.h>
#include "packet.h"
#include "global.h"

void cmd_info_flows();
void cmd_create_packet(const char *name);
void cmd_select_packet(const char *name);
void cmd_create_layer(char * typename);
void cmd_create_layers(char * layer_list);
void cmd_create_layer_child(char * typename);
void cmd_create_layer_children(char * layer_list);
void cmd_select_layer(const char * index);
void cmd_select_child_layer(const char * index);
void cmd_select_layer_path(const char * path);
void cmd_set_value(const char * left, const char *right);
void cmd_send_packet(const char * name, const char * num);
void cmd_set_payload_type(const char * type);
void cmd_info_layer_type(const char * name);
void _cmd_info_layer_content(layer_running_t * play, int level);
void cmd_info_flow_content();
void cmd_open_pcap_file(const char * file);
void cmd_open_device(const char * eth);
void cmd_close_pcap_file();
void cmd_close_device();
void do_sendto_device(char * buf,int len);
void do_sendto_pcap_file(char * buf,int len);
void cmd_write_update_layer_N(packet_running_t * ppkt,layer_running_t * play);
void cmd_write_layer_command(packet_running_t * ppkt,layer_running_t * play, FILE *fp);
void cmd_write_packet_command(char *name, FILE * fp);
void cmd_write_all(char *file);
void cmd_run_alias(char *out, char *scriptname, char *param_1, char *param_2, char *param_3, char *param_4,
	char *param_5, char *param_6, char *param_7, char *param_8);
void cmd_info_current_layer_struct_simple(layer_running_t * play, int n);
void cmd_info_current_flow_struct_simple();




